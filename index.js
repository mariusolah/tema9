const random = Math.floor(Math.random() * 101); 
const inputValue = document.getElementById('input');
const button = document.getElementById('button');
let display = document.getElementById('result');

button.addEventListener('click', () => {
    display.innerHTML = '';
    if (inputValue.value > random) {
        display.innerHTML = `Numarul ${inputValue.value} este mai mare decat ${random}`;
    }
    if (inputValue.value < random) {
        display.innerHTML = `Numarul ${inputValue.value} este mai mic decat ${random}`;
    }
    if (inputValue.value == random) {
        display.innerHTML = `Numarul ${inputValue.value} este egal cu ${random}`;
    }
})

